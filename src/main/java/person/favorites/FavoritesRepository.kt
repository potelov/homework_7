package person.favorites

import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import person.types.PersonWithAddress

internal class FavoritesRepository(private val personBackend: PersonBackend, private val favoritesDatabase: FavoritesDatabase) {


    fun loadFavorites(): Observable<List<PersonWithAddress>> {


        /**
         * Provide an observable that only emits a list of PersonWithAddress if they are marked as favorite ones.
         */

        return Observable.combineLatest(
                personBackend.loadAllPersons(),
                favoritesDatabase.favoriteContacts(),
                BiFunction { t1, t2 ->
                    var result = listOf<PersonWithAddress>()
                    t1.forEachIndexed { _, person ->
                        t2.forEachIndexed { _, i ->
                            if (person.person.id == i) {
                                result += person
                            }
                        }
                    }
                    result
                })
    }
}

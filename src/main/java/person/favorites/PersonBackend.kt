package person.favorites

import io.reactivex.Observable
import person.types.PersonWithAddress

internal interface PersonBackend {
    fun loadAllPersons(): Observable<List<PersonWithAddress>>
}

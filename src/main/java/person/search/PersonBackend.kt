package person.search

import io.reactivex.Observable
import person.types.Person

interface PersonBackend {

    fun searchfor(searchFor: String): Observable<List<Person>>
}

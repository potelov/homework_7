package fibonacci;

import io.reactivex.Observable;
import org.javatuples.Pair;

public class FibonacciExercises {

    public Observable<Integer> fibonacci(int n) {
        return Observable.range(0, n)
                .map(integer -> new Pair<>(0, 1))
                .scan((source, result) -> new Pair<>(source.getValue1(), source.getValue0() + source.getValue1()))
                .map(Pair::getValue0);
    }
}


